package com.kabkasik.server.mapper;

import com.kabkasik.server.models.dto.FindCacheDto;
import com.kabkasik.server.models.entities.FindCache;

public interface IFindCacheConverter {
    FindCache fromDto(FindCacheDto inFindCacheDto);
}
