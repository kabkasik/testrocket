package com.kabkasik.server.mapper;

import com.kabkasik.server.models.dto.RequestDto;
import com.kabkasik.server.models.dto.SuggestionsDto;
import com.kabkasik.server.models.entities.address.Address;
import com.kabkasik.server.models.entities.cache.Query;

import java.util.List;

//@Component
public interface ISuggestionsConverter {
    SuggestionsDto toDto(Query inQuery);
    SuggestionsDto toDto(List<Address> inAddressList);
    Query fromDto(SuggestionsDto inSuggestionsDto, RequestDto inRequestDto);
}
