package com.kabkasik.server.mapper;

import com.kabkasik.server.models.dto.DataDto;
import com.kabkasik.server.models.dto.RequestDto;
import com.kabkasik.server.models.dto.SuggestionDto;
import com.kabkasik.server.models.dto.SuggestionsDto;
import com.kabkasik.server.models.entities.IDictionaryEntity;
import com.kabkasik.server.models.entities.address.*;
import com.kabkasik.server.models.entities.cache.Query;
import com.kabkasik.server.models.entities.cache.QueryResult;
import com.kabkasik.server.repositories.IDictionaryRepository;
import com.kabkasik.server.service.AddressService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@RequiredArgsConstructor
public class SuggestionsConverterImpl implements ISuggestionsConverter {
    private final AddressService addressService;
    private final IDictionaryRepository dictionaryRepository;

    private Map<Class<?>, String[]> entityPerMethodsAddressToDataDto = new HashMap<Class<?>, String[]>() {{
        put(City.class, new String[]{"City", "City"});
        put(House.class, new String[]{"House", "House"});
        put(PostalCode.class, new String[]{"PostalCode", "Postal_code"});
        put(Region.class, new String[]{"Region", "Region"});
        put(Settlement.class, new String[]{"Settlement", "Settlement"});
        put(Street.class, new String[]{"Street", "Street"});
    }};


    @Override
    public SuggestionsDto toDto(Query inQuery) {

        List<Address> addressList = new ArrayList<Address>();
        for (QueryResult queryResult :
                inQuery.getQueryResultList()) {
            addressList.add(queryResult.getAddress());
        }

        return toDto(addressList);
    }

    public SuggestionsDto toDto(List<Address> inAddressList) {
        SuggestionsDto suggestionsDto = new SuggestionsDto();

        int count = inAddressList.size();
        suggestionsDto.setSuggestions(new SuggestionDto[count]);

        for (int i = 0; i < count; i++) {
            Address address = inAddressList.get(i);

            SuggestionDto suggestionDto = new SuggestionDto();
            suggestionDto.setValue(address.getName());

            DataDto dataDto = new DataDto();
            for (Map.Entry<Class<?>, String[]> entry : entityPerMethodsAddressToDataDto.entrySet()) {
                stringFromTo(dataDto, entry.getValue()[1], address, entry.getValue()[0], entry.getKey(), true);
            }

            suggestionDto.setData(dataDto);
            suggestionsDto.getSuggestions()[i] = suggestionDto;
        }

        return suggestionsDto;
    }

    @Override
    public Query fromDto(SuggestionsDto inSuggestionsDto, RequestDto inRequestDto) {
        Query query = new Query();
        query.setQueryResultList(new ArrayList<QueryResult>());
        for (SuggestionDto suggestionDto :
                inSuggestionsDto.getSuggestions()) {

            Address address = addressService.get(suggestionDto.getValue());
            DataDto dataDto = suggestionDto.getData();
            for (Map.Entry<Class<?>, String[]> entry : entityPerMethodsAddressToDataDto.entrySet()) {
                stringFromTo(address, entry.getValue()[0], dataDto, entry.getValue()[1], entry.getKey(), false);
            }

            QueryResult queryResult = new QueryResult(0, query, address);
            if (query.getQueryResultList() == null) {
                query.setQueryResultList(new ArrayList<QueryResult>());
            }
            query.getQueryResultList().add(queryResult);

        }

        query.setName(inRequestDto.getQuery());
        query.setCount(inRequestDto.getCount());
        query.setTime(LocalDateTime.now(ZoneId.of("UTC")));

        return query;
    }

    public void stringFromTo(Object toObj, String nameToMethod, Object fromObj, String nameFromMethod, Class<?> entityClazz, Boolean isToString) {
        Object obj = null;
        try {
            obj = fromObj.getClass().getMethod("get" + nameFromMethod).invoke(fromObj);
            if (isToString) {
                if (obj != null) {
                    obj = ((IDictionaryEntity) obj).getValue();
                }
            } else {
                obj = (IDictionaryEntity) dictionaryRepository.findByClazzAndValueOrNew(entityClazz, (String) obj);
            }
            toObj.getClass().getMethod("set" + nameToMethod, isToString ? String.class : obj.getClass()).invoke(toObj, isToString ? (String) obj : entityClazz.cast(obj));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
