package com.kabkasik.server.mapper;

import com.kabkasik.server.models.dto.FindCacheDto;
import com.kabkasik.server.models.entities.FindCache;
import com.kabkasik.server.models.entities.address.City;
import com.kabkasik.server.models.entities.address.Region;
import com.kabkasik.server.models.entities.address.Settlement;
import com.kabkasik.server.models.entities.address.Street;
import com.kabkasik.server.repositories.IDictionaryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class FindCacheConverterImpl implements IFindCacheConverter {
    private final IDictionaryRepository dictionaryRepository;

    @Override
    public FindCache fromDto(FindCacheDto inFindCacheDto) {
        Region region = dictionaryRepository.findByClazzAndValue(Region.class, inFindCacheDto.getRegion());
        City city = dictionaryRepository.findByClazzAndValue(City.class, inFindCacheDto.getCity());
        Settlement settlement = dictionaryRepository.findByClazzAndValue(Settlement.class, inFindCacheDto.getSettlement());
        Street street = dictionaryRepository.findByClazzAndValue(Street.class, inFindCacheDto.getStreet());

        return new FindCache(region, city, settlement, street, inFindCacheDto.getCount());
    }
}
