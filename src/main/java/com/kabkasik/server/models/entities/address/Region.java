package com.kabkasik.server.models.entities.address;

import com.kabkasik.server.models.entities.IDictionaryEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity(name = "region")
@AllArgsConstructor
@NoArgsConstructor
public class Region implements IDictionaryEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(unique = true)
    String value;
}
