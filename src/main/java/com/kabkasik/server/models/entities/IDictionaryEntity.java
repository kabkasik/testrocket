package com.kabkasik.server.models.entities;

public interface IDictionaryEntity {
    public void setValue(String value);
    public String getValue();
    public void setId(int value);
    public int getId();
}
