package com.kabkasik.server.models.entities.cache;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Entity(name = "query")
@AllArgsConstructor
@NoArgsConstructor
public class Query {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(unique = true)
    String name;

    @Column(unique = true)
    int count;

    LocalDateTime time;

    @JsonManagedReference
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "query")
    private List<QueryResult> queryResultList;

}
