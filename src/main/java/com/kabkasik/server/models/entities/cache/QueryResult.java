package com.kabkasik.server.models.entities.cache;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.kabkasik.server.models.entities.address.Address;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity(name = "query_result")
@AllArgsConstructor
@NoArgsConstructor
public class QueryResult {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "query_id")
    @JsonBackReference
    private Query query;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "address_id")
    @JsonBackReference
    private Address address;
}
