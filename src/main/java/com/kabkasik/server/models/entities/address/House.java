package com.kabkasik.server.models.entities.address;

import com.kabkasik.server.models.entities.IDictionaryEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity(name = "house")
@AllArgsConstructor
@NoArgsConstructor
public class House implements IDictionaryEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(unique = true)
    String value;
}
