package com.kabkasik.server.models.entities;

import com.kabkasik.server.models.entities.address.*;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
public class FindCache {
    Region region;
    City city;
    Settlement settlement;
    Street street;
    int maxCount;

    public FindCache(Region inRegion, City inCity, Settlement inSettlement, Street inStreet, int inMaxCount){
        region = inRegion;
        city = inCity;
        settlement = inSettlement;
        street = inStreet;
        maxCount = inMaxCount;
    }
}
