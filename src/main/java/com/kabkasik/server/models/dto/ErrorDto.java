package com.kabkasik.server.models.dto;

import com.kabkasik.server.exception.ErrorCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ErrorDto {

    private String message;
    private ErrorCode code;
}
