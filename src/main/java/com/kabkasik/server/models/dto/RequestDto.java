package com.kabkasik.server.models.dto;

import lombok.Data;

@Data
public class RequestDto {
    private String query;
    private int count;
}
