package com.kabkasik.server.models.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FindCacheDto {
    private String street;
    private String settlement;
    private String city;
    private String region;

    private int count;
}
