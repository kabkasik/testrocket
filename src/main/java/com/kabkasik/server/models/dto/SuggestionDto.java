package com.kabkasik.server.models.dto;


import lombok.Data;

@Data
public class SuggestionDto {
    private String value;
    private DataDto data;
}
