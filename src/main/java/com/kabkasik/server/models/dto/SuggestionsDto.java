package com.kabkasik.server.models.dto;

import lombok.Data;

@Data
public class SuggestionsDto {
    private SuggestionDto[] suggestions;
}