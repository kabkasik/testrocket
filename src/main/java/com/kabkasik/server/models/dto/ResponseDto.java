package com.kabkasik.server.models.dto;

import com.kabkasik.server.enums.Status;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ResponseDto<T> {

    private Status status;
    private boolean fromCache;
    private T data;
    private List<ErrorDto> errorDto = new ArrayList<>();

    public static ResponseDto<Void> failure(List<ErrorDto> inErrorDtoList) {
        ResponseDto<Void> responseDto = new ResponseDto<>();
        responseDto.setStatus(Status.ERROR);
        responseDto.setErrorDto(inErrorDtoList);
        return responseDto;
    }

    public static <T> ResponseDto<T> success(T data) {
        ResponseDto<T> responseDto = new ResponseDto<>();
        responseDto.setStatus(Status.OK);
        responseDto.setData(data);
        return responseDto;
    }
}
