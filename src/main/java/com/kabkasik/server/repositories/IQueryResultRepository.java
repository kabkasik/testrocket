package com.kabkasik.server.repositories;

import com.kabkasik.server.models.entities.cache.QueryResult;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IQueryResultRepository extends JpaRepository<QueryResult, Long> {
}
