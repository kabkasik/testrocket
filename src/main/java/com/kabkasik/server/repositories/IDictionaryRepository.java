package com.kabkasik.server.repositories;

public interface IDictionaryRepository {
    public <T> T findByClazzAndValue(Class<T> clazz, String value);
    public <T> T findByClazzAndValueOrNew(Class<T> clazz, String value);
}
