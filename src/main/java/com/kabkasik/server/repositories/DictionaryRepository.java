package com.kabkasik.server.repositories;


import com.kabkasik.server.exception.CommonException;
import com.kabkasik.server.exception.ErrorCode;
import com.kabkasik.server.models.entities.IDictionaryEntity;
import org.hibernate.proxy.HibernateProxy;
import org.springframework.stereotype.Repository;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public class DictionaryRepository implements IDictionaryRepository{
    @PersistenceContext
    private EntityManager entityManager;

    public <T> T findByClazzAndValue(Class<T> clazz, String value) {
        String nameTable = Optional.ofNullable(clazz)
                .map(clz -> clz.getAnnotation(Entity.class))
                .map(Entity::name)
                .orElseThrow(() -> new CommonException(ErrorCode.CANT_GET_ENTITY_FROM_CLASS, "error."));

        String query = "from " + nameTable + " c where c.value = ";

        query += Optional.ofNullable(value)
                .map(val -> "'" + val + "'")
                .orElse("null");

        List<T> resultList = entityManager.createQuery(query, clazz)
                .setMaxResults(1)
                .getResultList();
        if (resultList.size() == 0) {
            return null;
        }

        T newEntity = resultList.get(0);
        if (newEntity instanceof HibernateProxy) {
            newEntity = (T) ((HibernateProxy) newEntity).getHibernateLazyInitializer()
                    .getImplementation();
        }

        return newEntity;
    }


    public <T> T findByClazzAndValueOrNew(Class<T> clazz, String value) {
        return Optional.ofNullable(findByClazzAndValue(clazz, value))
                .orElseGet(() -> newInstance(clazz, value));
    }

    @Transactional
    protected <T> T newInstance(Class<T> clazz, String value) {
        T entity = null;
        try {
            entity = clazz.getConstructor().newInstance();
        } catch (Exception e) {
            throw new CommonException(ErrorCode.CANT_CREATE_NEW_CLASS, "error.");
        }
        ((IDictionaryEntity)entity).setValue(value);
        entityManager.persist(entity);
        return entity;
    }

}
