package com.kabkasik.server.repositories;

import com.kabkasik.server.models.entities.cache.Query;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface IQueryRepository extends JpaRepository<Query, Long> {
    Optional<Query> findByNameAndCount(String name, int Count);
    Optional<Query> findByNameAndCountAndTimeGreaterThanEqual(String name, int Count, LocalDateTime time);
    List<Query> findAllByTimeBefore(LocalDateTime time);
}
