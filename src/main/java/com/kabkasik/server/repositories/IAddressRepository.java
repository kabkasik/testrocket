package com.kabkasik.server.repositories;

import com.kabkasik.server.models.entities.address.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IAddressRepository extends JpaRepository<Address, Long> {
    Optional<Address> getOneByName(String name);
    List<Address> findAllByRegionAndCityAndSettlementAndStreet(Region region, City city, Settlement settlement, Street street);
}
