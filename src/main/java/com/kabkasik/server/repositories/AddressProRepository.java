package com.kabkasik.server.repositories;

import com.kabkasik.server.models.entities.FindCache;
import com.kabkasik.server.models.entities.IDictionaryEntity;
import com.kabkasik.server.models.entities.address.*;
import org.springframework.stereotype.Repository;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Repository
@Transactional
public class AddressProRepository implements IAddressProRepository{
    @PersistenceContext
    private EntityManager entityManager;

    public List<Address> findAll(FindCache inFindCache) {

        IDictionaryEntity[] arr = {inFindCache.getRegion(), inFindCache.getCity(), inFindCache.getSettlement(), inFindCache.getStreet()};

        List<String> collect = Stream.of(arr)
                .map(entity -> getValueNotNull(entity))
                .filter(s -> s != null)
                .collect(Collectors.toList());

        String query = "from address c where " + String.join(" and ", collect);

        List<Address> resultList = entityManager.createQuery(query)
                .setMaxResults(inFindCache.getMaxCount())
                .getResultList();

        return resultList;
    }

    private String getValueNotNull(IDictionaryEntity entity) {
        return Optional.ofNullable(entity)
                .map(e -> e.getValue())
                .map(e -> entity.getClass().getAnnotation(Entity.class))
                .map(e -> e.name())
                .map(name -> "c." + name + ".id = " + entity.getId())
                .orElse(null);
    }
}
