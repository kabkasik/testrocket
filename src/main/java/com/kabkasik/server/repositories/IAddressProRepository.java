package com.kabkasik.server.repositories;

import com.kabkasik.server.models.entities.FindCache;
import com.kabkasik.server.models.entities.address.Address;

import java.util.List;

public interface IAddressProRepository {
    List<Address> findAll(FindCache inFindCache);
}
