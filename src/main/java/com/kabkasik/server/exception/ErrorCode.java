package com.kabkasik.server.exception;

public enum ErrorCode {
    QUERY_NOT_FOUND,
    ADDRESS_NOT_FOUND,
    DICTIONARY_NOT_FOUND,
    CANT_GET_ENTITY_FROM_CLASS,
    CANT_CREATE_NEW_CLASS,
}
