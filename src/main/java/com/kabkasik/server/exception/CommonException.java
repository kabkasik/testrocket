package com.kabkasik.server.exception;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

public class CommonException extends RuntimeException {

    private static final long serialVersionUID = -5609378298660965276L;

    @Getter
    private final ErrorCode code;
    @Getter
    private final List<Object> params = new ArrayList<>();

    public CommonException(ErrorCode code, String message, Object id) {
        super(message);
        this.code = code;
        params.add(id);
    }

    public CommonException(ErrorCode code, String message) {
        super(message);
        this.code = code;
    }
}
