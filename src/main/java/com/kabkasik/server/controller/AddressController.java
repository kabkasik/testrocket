package com.kabkasik.server.controller;

import com.kabkasik.server.mapper.IFindCacheConverter;
import com.kabkasik.server.mapper.ISuggestionsConverter;
import com.kabkasik.server.models.dto.*;
import com.kabkasik.server.models.entities.FindCache;
import com.kabkasik.server.service.ProxyService;
import com.kabkasik.server.service.SuggestionsService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@Api
@CrossOrigin(methods = {RequestMethod.OPTIONS, RequestMethod.GET})
@RestController
@RequestMapping("address")
@RequiredArgsConstructor
public class AddressController {
    private final ProxyService proxyService;
    private final SuggestionsService suggestionsService;

    private final IFindCacheConverter findCacheConverter;
    private final ISuggestionsConverter suggestionsConverter;

    @PostMapping("/proxy")
    SuggestionsDto proxy(@RequestBody RequestDto requestDto, @RequestHeader(value = "Authorization", defaultValue = "Token 258b956660dc5087a20e4e51e15280773942a05d") String token) {
        return proxyService.getByQuery(requestDto, token);
    }


    // Поиск адреса по любому совпадению, незаполненные параметры игнорируются.
    @PostMapping("/any")
    SuggestionsDto any(@RequestBody FindCacheDto findCacheDto) {
        FindCache findCache = findCacheConverter.fromDto(findCacheDto);
        return suggestionsConverter.toDto(suggestionsService.findAllByFindCacheAny(findCache));
    }

    // Поиск адреса по конкретному набору полей.
    @PostMapping("/current")
    SuggestionsDto current(@RequestBody FindCacheDto findCacheDto) {
        FindCache findCache = findCacheConverter.fromDto(findCacheDto);
        return suggestionsConverter.toDto(suggestionsService.findAllByFindCache(findCache));
    }


}
