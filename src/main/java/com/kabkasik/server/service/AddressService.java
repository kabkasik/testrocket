package com.kabkasik.server.service;

import com.kabkasik.server.models.entities.address.Address;
import com.kabkasik.server.repositories.IAddressRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AddressService {
    private final IAddressRepository addressRepository;

    public Address get(String value){
        return addressRepository.getOneByName(value).orElseGet(() -> {
            Address address = new Address();
            address.setName(value);
            return addressRepository.save(address);
        });
    }
}
