package com.kabkasik.server.service;

import com.kabkasik.server.models.dto.RequestDto;
import com.kabkasik.server.models.dto.SuggestionsDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;


// Сервис для проксирования запроса на внешний сервис, если данных по этому запросу небыло больше 3 минут

@Service
@RequiredArgsConstructor
public class ProxyService {
    private final PostClient postClient;
    private final SuggestionsService suggestionsService;

    public SuggestionsDto getByQuery(RequestDto inRequestDto, String inToken) {
        return suggestionsService.getLast3Minutes(inRequestDto)
                .orElse(requestToAnotherServer(inRequestDto, inToken));
    }

    private SuggestionsDto requestToAnotherServer(RequestDto inRequestDto, String inToken) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("Accept", "application/json");
        headers.put("Authorization", inToken);
        SuggestionsDto suggestionsDto = postClient.getSuggestions(headers, inRequestDto);
        suggestionsService.save(suggestionsDto, inRequestDto);
        return suggestionsDto;
    }

}
