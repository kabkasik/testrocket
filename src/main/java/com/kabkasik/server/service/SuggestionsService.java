package com.kabkasik.server.service;

import com.kabkasik.server.mapper.ISuggestionsConverter;
import com.kabkasik.server.models.dto.RequestDto;
import com.kabkasik.server.models.dto.SuggestionsDto;
import com.kabkasik.server.models.entities.FindCache;
import com.kabkasik.server.models.entities.address.Address;
import com.kabkasik.server.models.entities.cache.Query;
import com.kabkasik.server.repositories.IAddressRepository;
import com.kabkasik.server.repositories.IAddressProRepository;
import com.kabkasik.server.repositories.IQueryRepository;
import com.kabkasik.server.repositories.IQueryResultRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Objects;
import java.util.Optional;


@Service
@RequiredArgsConstructor
@EnableScheduling
public class SuggestionsService {
    private final IQueryRepository queryRepository;
    private final IQueryResultRepository queryResultRepository;
    private final IAddressRepository addressRepository;
    private final ISuggestionsConverter suggestionsConverter;
    private final IAddressProRepository addressProRepository;

    public Optional<SuggestionsDto> getLast3Minutes(RequestDto inRequestDto) {
        return queryRepository.findByNameAndCountAndTimeGreaterThanEqual(inRequestDto.getQuery(), inRequestDto.getCount(), LocalDateTime.now(ZoneId.of("UTC")).minusHours(3))
                .map(query -> suggestionsConverter.toDto(query));
    }

    @Transactional
    public void save(SuggestionsDto inSuggestionsDto, RequestDto inRequestDto) {
        int lastId = 0;
        Optional<Integer> queryId = queryRepository.findByNameAndCount(inRequestDto.getQuery(), inRequestDto.getCount())
                .filter(Objects::nonNull)
                .map(query -> {
                    queryResultRepository.deleteAll(query.getQueryResultList());
                    return query.getId();
                });
        if(!queryId.isEmpty()) lastId = queryId.get();
        Query query = suggestionsConverter.fromDto(inSuggestionsDto, inRequestDto);
        query.setId(lastId);
        queryRepository.save(query);
        if (query.getQueryResultList() != null) {
            queryResultRepository.saveAll(query.getQueryResultList());
        }
    }

    public List<Address> findAllByFindCacheAny(FindCache findCache) {
        return addressProRepository.findAll(findCache);
    }

    public List<Address> findAllByFindCache(FindCache findCache) {
        return addressRepository.findAllByRegionAndCityAndSettlementAndStreet(findCache.getRegion(), findCache.getCity(), findCache.getSettlement(), findCache.getStreet());
    }


    @Transactional
    @Scheduled(cron = "0 0 * * * ?")
    public void scheduleTaskUsingCron() {
        long now = System.currentTimeMillis() / 1000;
        System.out.println("schedule tasks using cron jobs - " + now);
        LocalDateTime localDateTime = LocalDateTime.now(ZoneId.of("UTC")).minusMinutes(1);
        List<Query> queryList = queryRepository.findAllByTimeBefore(localDateTime);
        if (queryList.size() > 0) {
            for (Query query :
                    queryList) {
                if (query.getQueryResultList() != null) {
                    queryResultRepository.deleteAll(query.getQueryResultList());
                    query.setQueryResultList(null);
                }

            }
            queryRepository.deleteAll(queryList);
        }
    }

}
