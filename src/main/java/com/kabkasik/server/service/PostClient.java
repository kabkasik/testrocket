package com.kabkasik.server.service;

import com.kabkasik.server.models.dto.RequestDto;
import com.kabkasik.server.models.dto.SuggestionsDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

@FeignClient(name = "dadata", url = "${grade-service-url}")
public interface PostClient {
    @RequestMapping(method = RequestMethod.POST)
    SuggestionsDto getSuggestions(@RequestHeader Map<String, String> headerMap, @RequestBody RequestDto inRequestDto);
}