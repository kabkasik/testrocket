--//-- СЛОВАРИ --//--
---table postal_code--------
create table postal_code
(
    id    serial  not null
        constraint postal_code_pk
            primary key,
    value varchar
);

create unique index postal_code_value_uindex
    on postal_code (value);

---table region--------
create table region
(
    id    serial  not null
        constraint region_pk
            primary key,
    value varchar
);

create unique index region_value_uindex
    on region (value);

---table city--------
create table city
(
    id    serial  not null
        constraint city_pk
            primary key,
    value varchar
);

create unique index city_value_uindex
    on city (value);

---table settlement--------
create table settlement
(
    id    serial  not null
        constraint settlement_pk
            primary key,
    value varchar
);

create unique index settlement_value_uindex
    on settlement (value);

---table street--------
create table street
(
    id    serial  not null
        constraint street_pk
            primary key,
    value varchar
);

create unique index street_value_uindex
    on street (value);

---table house--------
create table house
(
    id    serial  not null
        constraint house_pk
            primary key,
    value varchar
);

create unique index house_value_uindex
    on house (value);

---address---
create table address
(
	id serial
		constraint address_pk
			primary key,
	name varchar not null,
	house_id integer references house(id),
	street_id integer references street(id),
	settlement_id integer references settlement(id),
	city_id integer references city(id),
	region_id integer references region(id),
	postal_code_id integer references postal_code(id)
);

create unique index address_name_uindex
	on address (name);

--//-- КЭШ --//--
---query---
create table query
(
	id serial
		constraint query_pk
			primary key,
	name varchar not null,
	count integer,
	time timestamp default (now() at time zone 'utc')
);
create unique index query_name_and_count_uindex
	on query (name, count);
create index query__index_time
    on query (time);

---query_result---
create table query_result
(
	id serial
		constraint query_result_pk
			primary key,
	query_id integer references query(id),
	address_id integer references address(id)
);