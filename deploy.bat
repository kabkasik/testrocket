@echo off
set v_address=dagon@192.168.6.144
set v_path=build2
ssh %v_address% "cd ~/%v_path% && docker-compose down && cd .. && rm -R ~/%v_path%"
echo --------------Deleting done--------------
scp -r !build/ %v_address%:~/%v_path%/
echo --------------Copy done--------------
ssh %v_address% "cd ~/%v_path% && chmod +x ~/%v_path%/start.sh && ./start.sh"
echo --------------Start done--------------
pause